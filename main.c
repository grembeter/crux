#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <lib/print/print.h>
#include <lib/cmd/cmd.h>
#include <lib/version.h>

void __io_putchar(int data);

const char banner[] = "####                             \n\
 #        __ ____  __ __ __ __                          \n\
 #       /  |    \\|  |  |  |  |                        \n\
 #      /  /|  D  |  |  |  |  |                         \n\
 #     /  / |    /|  |  |_   _|                         \n\
 #    /   \\_|    \\|  :  |     |                       \n\
 #    \\     |  .  |     |  |  |                        \n\
 #     \\____|__|\\__\\__|_|__|__|                      \n\
 #                                                      \n\
##                        " CRX_VERSION "\n";


#if 0

#include <libopencm3/cm3/nvic.h>
#include <stddef.h>
#include <stdint.h>

// Storage for our monotonic system clock.
// Note that it needs to be volatile since we're modifying it from an interrupt.
static volatile unsigned long _millis = 0;

// Get the current value of the millis counter
uint64_t millis(void)
{
    return _millis;
}

// This is our interrupt handler for the systick reload interrupt.
// The full list of interrupt services routines that can be implemented is
// listed in libopencm3/include/libopencm3/stm32/f0/nvic.h
void sys_tick_handler(void)
{
    // Increment our monotonic clock
    _millis++;
}

uint64_t millis(void);
void delay(uint64_t duration);

static void systick_setup(void)
{
    // Set the systick clock source to our main clock
    systick_set_clocksource(STK_CSR_CLKSOURCE_AHB);
    // Clear the Current Value Register so that we start at 0
    STK_CVR = 0;
    // In order to trigger an interrupt every millisecond, we can set the reload
    // value to be the speed of the processor / 1000 - 1
    systick_set_reload(rcc_ahb_frequency / 1000 - 1);
    // Enable interrupts from the system tick clock
    systick_interrupt_enable();
    // Enable the system tick counter
    systick_counter_enable();
}

// Delay a given number of milliseconds in a blocking manner
void delay(uint64_t duration)
{
    const uint64_t until = millis() + duration;
    while (millis() < until) {
        ;
    }
}

#endif  /* 0 */

static void usart2_setup(void)
{
    // For the peripheral to work, we need to enable it's clock
    rcc_periph_clock_enable(RCC_USART2);
    // From the datasheet for the STM32F0 series of chips (Page 30, Table 11)
    // we know that the USART3 peripheral has it's TX line connected as
    // alternate function 1 on port A pin 9.
    // In order to use this pin for the alternate function, we need to set the
    // mode to GPIO_MODE_AF (alternate function). We also do not need a pullup
    // or pulldown resistor on this pin, since the peripheral will handle
    // keeping the line high when nothing is being transmitted.
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO2);
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO3);
    // Now that we have put the pin into alternate function mode, we need to
    // select which alternate function to use. PA9 can be used for several
    // alternate functions - Timer 15, USART3 TX, Timer 1, and on some devices
    // I2C. Here, we want alternate function 1 (USART3_TX)
    gpio_set_af(GPIOA, GPIO_AF7, GPIO2);
    gpio_set_af(GPIOA, GPIO_AF7, GPIO3);
    // Now that the pins are configured, we can configure the USART itself.
    // First, let's set the baud rate at 115200
    usart_set_baudrate(USART2, 115200);
    // Each datum is 8 bits
    usart_set_databits(USART2, 8);
    // No parity bit
    usart_set_parity(USART2, USART_PARITY_NONE);
    // One stop bit
    usart_set_stopbits(USART2, USART_CR2_STOPBITS_1);
    // For a debug console, we only need unidirectional transmit
    usart_set_mode(USART2, USART_MODE_TX_RX);
    // No flow control
    usart_set_flow_control(USART2, USART_FLOWCONTROL_NONE);
    // Enable the peripheral
    usart_enable(USART2);

    // Optional extra - disable buffering on stdout.
    // Buffering doesn't save us any syscall overhead on embedded, and
    // can be the source of what seem like bugs.
    // setbuf(stdout, NULL);
}

void __io_putchar(int data)
{
    usart_send_blocking(USART2, data);
}

int main(void)
{
    // First, let's ensure that our clock is running off the high-speed
    // internal oscillator (HSI) at 168MHz
    rcc_clock_setup_pll(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

    // Since our LED is on GPIO bank D, we need to enable
    // the peripheral clock to this GPIO bank in order to use it.
    rcc_periph_clock_enable(RCC_GPIOD);
    rcc_periph_clock_enable(RCC_GPIOA);

    // Initialize our systick timer
    // systick_setup();

    // Our test ORANGE LED is connected to Port D pin 13, so let's set it as output
    gpio_mode_setup(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO13);

    // Initialize our UART
    usart2_setup();

    print("\r%s", banner);

    cmd_task();

    return 0;
}
