#include <lib/print/print.h>
#include <stdarg.h>

extern void __io_putchar(int data);

static void print_num(unsigned base, unsigned long long x, int sign,
                      int width, char flag)
{
    unsigned char num[32];
    int len = 0;
    int pad = 0;

    if (sign < 0) {
        x = -x;
    }

    if (x == 0) {
        num[len++] = '0';
    }

    while (x > 0) {
        char m = (char)(x % base);
        num[len++] = (unsigned char)((m >= 10) ? (m - 10 + 'a') : (m + '0'));
        x = x / base;
    }

    pad = width - len;

    switch (flag) {
        case '0': {
            if (sign < 0) {
                __io_putchar('-');
                --pad;
            }
            while (pad-- > 0) {
                __io_putchar('0');
            }
            while (len) {
                __io_putchar(num[--len]);
            }
        }; break;
        case '-': {
            __io_putchar((sign < 0) ? ('-') : (' '));
            while (len) {
                __io_putchar(num[--len]);
            }
            while (--pad > 0) {
                __io_putchar(' ');
            }
        }; break;
        case '+':
        case ' ': {
            while (--pad > 0) {
                __io_putchar(' ');
            }
            __io_putchar((sign < 0) ? ('-') : (flag));
            while (len) {
                __io_putchar(num[--len]);
            }
        }; break;
        default: {
            if (sign < 0) {
                --pad;
            }
            while (pad-- > 0) {
                __io_putchar(' ');
            }
            if (sign < 0) {
                __io_putchar('-');
            }
            while (len) {
                __io_putchar(num[--len]);
            }
        }
    }
}

static void print_str(const char *str, int width, char flag)
{
    int pad;
    const char *p;

    if (!str) {
        return;
    }

    for (pad = width, p = str; *p != '\0'; ++p) {
        --pad;
    }

    if (flag == '-') {
        while (*str) {
            __io_putchar(*str++);
        }
        while (pad-- > 0) {
            __io_putchar(' ');
        }
    } else {
        while (pad-- > 0) {
            __io_putchar(' ');
        }
        while (*str) {
            __io_putchar(*str++);
        }
    }
}

void print(const char *fmt, ...)
{
    const char *p = fmt;
    const char *f = p;
    const char *pwidth;
    va_list argp;
    char length = 'W';
    char flag = '.';
    int width_len;
    int width;
    char c;

    enum {
        WANT_ORDINARY_CHAR,
        WANT_FLAG,
        WANT_WIDTH,
        WANT_LENGTH,
        WANT_SPECIFIER
    } want = WANT_ORDINARY_CHAR;

    va_start(argp, fmt);

    for (p = fmt; *p != '\0'; ++p) {
        c = *p;

        if (want == WANT_ORDINARY_CHAR) {
            if (c == '%') {
                want = WANT_FLAG;
                f = p;
                flag = '.';
                length = 'W';
                width_len = 0;
                width = 0;
            } else {
                __io_putchar(c);
            }

            continue;
        }

        if (want == WANT_FLAG) {
            want = WANT_WIDTH;

            switch (c) {
                case ' ':
                case '#':
                case '0':
                case '-':
                case '+': {
                    flag = c;
                    continue;
                }; break;
            }
        }

        if (want == WANT_WIDTH) {
            int m;
            int l;

            if (((c >= '1') && (c <= '9'))
                || ((c == '0') && (width_len > 0)))
            {
                pwidth = p;
                ++width_len;
                continue;
            }

            l = width_len;
            m = 1;

            while (l-- > 0) {
                width += m * (*pwidth-- - '0');
                m *= 10;
            }

            want = WANT_LENGTH;
        }

        if (want == WANT_LENGTH) {
            if (c == length) {
                length -= 'a' - 'A';
                want = WANT_SPECIFIER;
                continue;
            }

            switch (c) {
                case 'h':
                case 'l': {
                    length = c;
                    continue;
                }; break;
            }

            want = WANT_SPECIFIER;
        }

        if (want == WANT_SPECIFIER) {
            switch (c) {
                case '%': {
                    __io_putchar(c);
                }; break;
                case 'c': {
                    int arg = va_arg(argp, int);
                    __io_putchar(arg);
                }; break;
                case 's': {
                    char *arg = va_arg(argp, char *);
                    print_str(arg, width, flag);
                }; break;
                case 'd':
                case 'i': {
                    long long arg;

                    switch (length) {
                        case 'H': {
                            arg = (char)va_arg(argp, int);
                        }; break;
                        case 'h': {
                            arg = (short)va_arg(argp, int);
                        }; break;
                        case 'l': {
                            arg = va_arg(argp, long);
                        }; break;
                        case 'L': {
                            arg = va_arg(argp, long long);
                        }; break;
                        default: {
                            arg = va_arg(argp, int);
                        }
                    }

                    print_num(10, (unsigned long long)arg, (arg < 0) ? (-1) : (1), width, flag);
                }; break;
                case 'u':
                case 'x': {
                    unsigned long long arg = 0;

                    switch (length) {
                        case 'H': {
                            arg = (unsigned char)va_arg(argp, unsigned int);
                        }; break;
                        case 'h': {
                            arg = (unsigned short)va_arg(argp, unsigned int);
                        }; break;
                        case 'W': {
                            arg = va_arg(argp, unsigned int);
                        }; break;
                        case 'l': {
                            arg = va_arg(argp, unsigned long);
                        }; break;
                        case 'L': {
                            arg = va_arg(argp, unsigned long long);
                        }; break;
                    }

                    print_num(((c == 'u') ? 10 : 16), arg, 1, width, flag);
                }; break;
                default: {
                    while (f <= p) {
                        __io_putchar(*f++);
                    }
                }
            }

            want = WANT_ORDINARY_CHAR;
        }
    }

    va_end(argp);
}
