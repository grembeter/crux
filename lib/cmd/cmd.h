#pragma once

#define CMD_NAME_LEN (15)

extern struct cmd_table __cmd_tab_start;
extern struct cmd_table __cmd_tab_end;

struct cmd_table {
    const char name[CMD_NAME_LEN+1];
    const char usage[256];
    int (*run)(struct cmd_table *, int, const char **);
};

#define CRX_CMDT_SECTION ".cmd_tab."
#define CRX_CMDT(_name,_usage)                                                                   \
    extern struct cmd_table cmdt_##_name __attribute__ ((section (CRX_CMDT_SECTION#_name)));     \
    struct cmd_table cmdt_##_name = {                                                            \
        .name = #_name,                                                                          \
        .usage = _usage,                                                                         \
        .run = cmd_##_name                                                                       \
    }


#define CRX_CMD_DECLARATION(_name)                                              \
    int cmd_##_name(struct cmd_table *cmd, int argc, const char **argv)

CRX_CMD_DECLARATION(help);
CRX_CMD_DECLARATION(mem);
CRX_CMD_DECLARATION(version);
