/**
 * @file cmd_version.c
 */

#include <lib/print/print.h>
#include <lib/version.h>
#include <string.h>
#include "cmd.h"

#define CMD_USAGE  \
    "help [cmd]\n" \
    " give a short usage message"

int cmd_help(struct cmd_table *cmd, int argc, const char **argv)
{
    struct cmd_table *cmd_tab = &__cmd_tab_start;
    int ret;

    if (argc == 1) {
        while (cmd_tab < &__cmd_tab_end) {
            print("%s\n", cmd_tab->name);
            ++cmd_tab;
        }

        ret = 0;
    } else {
        ret = 1;

        while (cmd_tab < &__cmd_tab_end) {
            if (0 == strncmp(argv[1], cmd_tab->name, sizeof(cmd_tab->name))) {
                print("usage: %s\n", cmd_tab->usage);
                ret = 0;
                break;
            }

            ++cmd_tab;
            ret = 1;
        }

        if (ret != 0) {
            print("error: cmd not found: %s\n", argv[1]);
        }
    }

    return ret;
}

CRX_CMDT(help, CMD_USAGE);
