/**
 * @file cmd_version.c
 */

#include <lib/print/print.h>
#include <lib/version.h>
#include "cmd.h"

#define CMD_USAGE  \
    "version\n"                                                                 \
    " print firmware version"

int cmd_version(struct cmd_table *cmd, int argc, const char **argv)
{
    print(CRX_VERSION "\n");

    return 0;
}

CRX_CMDT(version, CMD_USAGE);
