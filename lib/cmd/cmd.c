#include <libopencm3/stm32/usart.h>
#include <lib/print/print.h>
#include <lib/cmd/cmd.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>

#include "cmd.h"

#define INPUT_LEN (127)
#define ARGV_LEN (8)

struct cmd_line {
    uint32_t argc;
    const char *argv[ARGV_LEN];
    char buf[INPUT_LEN+1];
} g_cmdline;

void __io_putchar(int data);

static void parse_line(uint32_t len)
{
    uint32_t i;

    g_cmdline.argc = 0;

    print(" DBG len=%lu\n", len);

    for (i = 1; i < len; ++i) {
        if (g_cmdline.buf[i] == '\0') {
            continue;
        }

        if (g_cmdline.buf[i-1] != '\0') {
            continue;
        }

        if (g_cmdline.argc < 8) {
            g_cmdline.argv[g_cmdline.argc] = &g_cmdline.buf[i];
            g_cmdline.argc += 1;
        }
    }

    for (i = 0; i < g_cmdline.argc; i++) {
        print(" DBG cmd: %02lu: %s\n", i, g_cmdline.argv[i]);
    }
}

static void run_cmd(void)
{
    struct cmd_table *cmd = &__cmd_tab_start;

    while (cmd < &__cmd_tab_end) {
        if (0 == strncmp(cmd->name, g_cmdline.argv[0], sizeof(cmd->name))) {
            cmd->run(cmd, g_cmdline.argc, g_cmdline.argv);
            break;
        }

        ++cmd;
    }
}

int cmd_task()
{
    struct cmd_table cmd;

    while (true) {
        uint16_t c = '\0';
        int len = 1;

        print("## ");

        while ((c = usart_recv_blocking(USART2)) != '\n') {
            if ((c >= ' ') && (c < 0x7f)) {
                if (len == INPUT_LEN) {
                    continue;
                }

                __io_putchar(c);
                g_cmdline.buf[len++] = (c == ' ') ? '\0' : c;
            } else if (c == 0x7f) {
                if (len == 1) {
                    continue;
                }

                print("\x1b[1D\x1b[K");
                len -= 1;
            }
        }

        __io_putchar('\n');
        g_cmdline.buf[len] = '\0';

        print(" DBG processing: [%d] %s\n", len, g_cmdline.buf);

        parse_line(len);
        run_cmd();
    }

    return 0;
}
