/**
 * @file cmd_version.c
 */

#include <lib/print/print.h>
#include <string.h>
#include <stdlib.h>
#include "cmd.h"

#define CMD_USAGE                                                                 \
    "mem <action> [args]\n"                                                       \
    " show <addr> <nmemb>"

int cmd_mem(struct cmd_table *cmd, int argc, const char **argv)
{
    if (argc == 1) {
        print("%s\n", cmd->usage);
        return 0;
    }

    if (0 == strncmp(argv[1], "show", 5U)) {
        unsigned long bsize = 1;
        unsigned long addr;
        unsigned long nmemb;
        unsigned long p;

        if (argc != 4) {
            print("error: provided %d arguments, while expected only 4\n", argc);
            print("%s\n", cmd->usage);
            return -1;
        }

        addr = strtoul(argv[2], NULL, 0);
        nmemb = strtoul(argv[3], NULL, 10);

        print(" DBG addr=0x%lx\n", addr);
        print(" DBG nmemb=%lu\n", nmemb);

        for (p = addr; nmemb > 0; --nmemb) {
            print("%02x ", *(unsigned char *)p);
            p += bsize;

            if (((p - addr) & 15) == 0) {
                print("\n");
            }
        }

        print("\n");
    }

    return 0;
}

CRX_CMDT(mem, CMD_USAGE);
